const app = require('express')();
const functions = require('firebase-functions');
const port = 3000;

app.get('/', (req, res) => {
    res.json({
        'name' : 'Phatipan Kerdsurasit',
        'age' : '25',
    });
})

app.get('/scg', (req, res) => {
    let values = [];
    let counter = 0;
    let start = 3;
    let i = 0;
    while (i < 7) {
        start += counter
        counter += 2
        values.push(start)
        i++;
    }
    res.send(values);
})

exports.helloWorld = functions.https.onRequest(app);
