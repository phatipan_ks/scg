import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue';
import './assets/css/app.scss';
import './assets/css/animate.css';
import './assets/fontawesome/css/all.min.css';
import  VueScrollTo from  'vue-scrollto'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
 
// You can also pass in the default options
Vue.use(VueScrollTo, {
     container: "body",
     duration: 500,
     easing: "ease",
     offset: -150,
     force: true,
     cancelable: true,
     onStart: false,
     onDone: false,
     onCancel: false,
     x: false,
     y: true
})

Vue.config.productionTip = false
Vue.use(VueScrollTo)
Vue.use(BootstrapVue);
Vue.use(VueAxios, axios)

Vue.config.productionTip = false
Vue.directive('scroll', {
    inserted: function(el, binding) {
      let f = function(evt) {
        if (binding.value(evt, el)) {
          window.removeEventListener('scroll', f);
        }
      };
      window.addEventListener('scroll', f);
    },
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
